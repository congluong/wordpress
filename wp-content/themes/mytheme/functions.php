<?php

function myTheme_ProductPostType() {
	register_post_type( 'products', [
		'labels' => [
			'name' => 'Sản phẩm',
			'singular_name' => 'Product',
			'add_new' => 'Thêm mới',
			'add_new_item' => 'Thêm mới sản phẩm',
			'edit_item' => 'Sửa'
		],
		'public' => true,
		'show_ui' => true,
		'supports' => ['title', 'editor', 'thumbnail', 'post-thumbnails']
	]);
}

add_action('init', 'myTheme_ProductPostType');

function myTheme_MetaBoxProductInfo() {
	add_meta_box('product-info' , 'Product Info', function() {
		global $post;

		echo '
		<table class="form-table">
			<tr>
				<td>Product Name</td>
				<td><input type="text" name="pro_name" value="'. get_post_meta( $post->ID, 'pro_name', true ) .'"></td>
			</tr>
			<tr>
				<td>Product Price In</td>
				<td><input type="text" name="pro_price_in" value="'. get_post_meta( $post->ID, 'pro_price_in', true ) .'"></td>
			</tr>
			<tr>
				<td>Product Price Out</td>
				<td><input type="text" name="pro_price_out" value="'. get_post_meta( $post->ID, 'pro_price_out', true ) .'"></td>
			</tr>
		</table>';
	}, 'products');
}

add_action('add_meta_boxes' , 'myTheme_MetaBoxProductInfo');

function myTheme_SaveMetaBoxes($post_id) {
	update_post_meta( $post_id, 'pro_name', $_POST['pro_name']);
	update_post_meta( $post_id, 'pro_price_in', $_POST['pro_price_in']);
	update_post_meta( $post_id, 'pro_price_out', $_POST['pro_price_out']);
}

add_action('save_post', 'myTheme_SaveMetaBoxes');

add_theme_support('post-thumbnails' );

# Sửa thẻ title nếu là trang chủ
add_filter( 'wp_title', 'baw_hack_wp_title_for_home' );

function baw_hack_wp_title_for_home( $title )
{
  	if( empty( $title ) && ( is_home() || is_front_page() ) ) {
    	return __( 'Home', 'theme_domain' ) . ' | ' . get_bloginfo( 'description' );
  	}
  	return $title;
}


/**
 * Create a taxonomy
 *
 * @uses  Inserts new taxonomy object into the list
 * @uses  Adds query vars
 *
 * @param string  Name of taxonomy object
 * @param array|string  Name of the object type for the taxonomy object.
 * @param array|string  Taxonomy arguments
 * @return null|WP_Error WP_Error if errors, otherwise null.
 */
function taxonomy_products() {

	$labels = array(
		'name'					=> _x( 'Nhom san pham', 'Nhom san pham', 'text-domain' ),
		'singular_name'			=> _x( 'Nhom san pham', 'Taxonomy singular name', 'text-domain' ),
		'search_items'			=> __( 'Search Plural Name', 'text-domain' ),
		'popular_items'			=> __( 'Popular Plural Name', 'text-domain' ),
		'all_items'				=> __( 'All Plural Name', 'text-domain' ),
		'parent_item'			=> __( 'Parent Singular Name', 'text-domain' ),
		'parent_item_colon'		=> __( 'Parent Singular Name', 'text-domain' ),
		'edit_item'				=> __( 'Edit Singular Name', 'text-domain' ),
		'update_item'			=> __( 'Update Singular Name', 'text-domain' ),
		'add_new_item'			=> __( 'Add New Singular Name', 'text-domain' ),
		'new_item_name'			=> __( 'New Singular Name Name', 'text-domain' ),
		'add_or_remove_items'	=> __( 'Add or remove Plural Name', 'text-domain' ),
		'choose_from_most_used'	=> __( 'Choose from most used text-domain', 'text-domain' ),
		'menu_name'				=> __( 'Nhom san pham', 'text-domain' ),
	);

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => false,
		'hierarchical'      => true,
		'show_tagcloud'     => true,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => true,
		'query_var'         => true,
		'capabilities'      => array(),
	);

	register_taxonomy( 'taxonomy-products', array( 'products' ), $args );
}

add_action( 'init', 'taxonomy_products' );