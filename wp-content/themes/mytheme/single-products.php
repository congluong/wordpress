<?php
get_header( );
?>

<div>
	<p><?php the_title(); ?></p>
	<?php the_permalink(); ?>
	<?php the_post_thumbnail(); ?>
	<?php the_content(); ?>

</div>