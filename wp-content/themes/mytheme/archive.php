<?php

$category = get_queried_object();


echo '<h1>This is category ' . get_cat_name($category->term_id) . '</h1>';

echo 'Total posts : ' . $category->count;

// Get all post in this category

$posts = get_posts(['category' => $category->term_id, 'posts_per_page' => 1, 'offset' => get_query_var( 'page' ) - 1] );

echo '<h2>Posts</h2>';

foreach($posts as $post) {
	echo '<a href="'. get_permalink($post->ID) .'">'. get_the_title($post) .'</a><br/>';
}

echo '<h3>Pagination</h3>';

$args = array(
	'base'         => '%_%',
	'format'       => '?page=%#%',
	'total'        => $category->count,
	'current'      => get_query_var('page'),
	'show_all'     => False,
	'end_size'     => 10,
	'mid_size'     => 10,
	'prev_next'    => True,
	'prev_text'    => __('« Previous'),
	'next_text'    => __('Next »'),
	'type'         => 'plain',
	'add_args'     => False,
	'add_fragment' => '',
	'before_page_number' => '',
	'after_page_number' => ''
);

echo paginate_links($args);