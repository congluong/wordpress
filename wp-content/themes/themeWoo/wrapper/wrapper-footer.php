<?php /**
* Wrapper Name: Footer
*/
?><div class="footer-widgets">
	<div class="row" data-motopress-id="520a2c647a3b7" data-motopress-file="wrapper/wrapper-footer.php">
		<div class="span3" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-1" data-motopress-id="520a2c647a3b7" data-motopress-file="wrapper/wrapper-footer.php">
			<?php dynamic_sidebar("footer-sidebar-1"); ?></div>
		<div class="span3" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-2" data-motopress-id="520a2c647a3b7" data-motopress-file="wrapper/wrapper-footer.php">
			<?php dynamic_sidebar("footer-sidebar-2"); ?></div>
		<div class="span3" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-3" data-motopress-id="520a2c647a3b7" data-motopress-file="wrapper/wrapper-footer.php">
			<?php dynamic_sidebar("footer-sidebar-3"); ?></div>
		<div class="span3" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-4" data-motopress-id="520a2c647a3b7" data-motopress-file="wrapper/wrapper-footer.php">
			<?php dynamic_sidebar("footer-sidebar-4"); ?></div>
	</div>
</div>
<div class="copyright">
	<div class="row" data-motopress-id="520a2c647a3b7" data-motopress-file="wrapper/wrapper-footer.php">
		<div class="span6" data-motopress-type="static" data-motopress-static-file="static/static-footer-text.php" data-motopress-id="520a2c647a3b7" data-motopress-file="wrapper/wrapper-footer.php">
			<?php get_template_part("static/static-footer-text"); ?></div>
		<div class="span6" data-motopress-type="static" data-motopress-static-file="static/static-footer-nav.php" data-motopress-id="520a2c647a3b7" data-motopress-file="wrapper/wrapper-footer.php">
			<?php get_template_part("static/static-footer-nav"); ?></div>
	</div>
</div>