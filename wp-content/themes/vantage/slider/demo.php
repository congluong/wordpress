<div class="flexslider" id="metaslider-demo">
	<ul class="slides">

		<li>
			<div class="content">
				<img src="<?php echo get_template_directory_uri() ?>/slider/backgrounds/camera.jpg" width="1080" height="420" alt="<?php esc_attr_e('Demo Slide', 'vantage') ?>" />
			</div>

			<img src="<?php echo get_template_directory_uri() ?>/slider/backgrounds/blackbg.jpg" class="msDefaultImage" width="1080" height="420" alt="<?php esc_attr_e('Demo Slide', 'vantage') ?>" />
		</li>

		<li>
			<div class="content">
				<img src="<?php echo get_template_directory_uri() ?>/slider/backgrounds/maychamcong.jpg" width="1080" height="420" alt="<?php esc_attr_e('Demo Slide', 'vantage') ?>" />
			</div>

			<img src="<?php echo get_template_directory_uri() ?>/slider/backgrounds/blackbg.jpg" class="msDefaultImage" width="1080" height="420" alt="<?php esc_attr_e('Demo Slide', 'vantage') ?>" />
		</li>

		<li>
			<div class="content">
				<img src="<?php echo get_template_directory_uri() ?>/slider/backgrounds/maytinh.jpg" width="1080" height="420" alt="<?php esc_attr_e('Demo Slide', 'vantage') ?>" />
			</div>

			<img src="<?php echo get_template_directory_uri() ?>/slider/backgrounds/blackbg.jpg" class="msDefaultImage" width="1080" height="420" alt="<?php esc_attr_e('Demo Slide', 'vantage') ?>" />
		</li>

	</ul>
</div>