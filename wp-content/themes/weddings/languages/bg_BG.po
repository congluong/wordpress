msgid ""
msgstr ""
"Project-Id-Version: exclusive\n"
"POT-Creation-Date: 2014-01-19 16:13+0400\n"
"PO-Revision-Date: 2014-01-19 17:53+0400\n"
"Last-Translator: dianna <dianna.arakelyan@gmail.com>\n"
"Language-Team: \n"
"Language: Afrikaans\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.3\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: C:\\wamp\\www\\theme_test\\wp-content\\themes"
"\\exclusive\n"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/404.php:23
msgid "Type here"
msgstr "Въведете тук"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/404.php:24
#: C:\wamp\www\theme_test\wp-content\themes\exclusive/search.php:37
msgid "Search"
msgstr "Търсене"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/category.php:16
msgid "Archive for the"
msgstr "Архив за"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/category.php:16
msgid "Category"
msgstr "Категория"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/category.php:18
msgid "Posts Tagged"
msgstr "Мнения Маркирани"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/category.php:20
#: C:\wamp\www\theme_test\wp-content\themes\exclusive/category.php:22
#: C:\wamp\www\theme_test\wp-content\themes\exclusive/category.php:24
msgid "Archive for"
msgstr "Архив за"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/category.php:26
msgid "Author Archive"
msgstr "Автор Архив"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/category.php:28
msgid "Blog Archives"
msgstr "Блог Архиви"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/comments.php:27
msgid "&#8249; Older comments"
msgstr "<По-стари коментари"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/comments.php:28
msgid "Newer comments &#8250;"
msgstr "По-нови коментари>"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/comments.php:39
msgid "Pings&#47;Trackbacks"
msgstr "Пинг / Trackbacks"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/comments.php:42
#, php-format
msgid "%1$d %2$s for \"%3$s\""
msgstr "%1$d %2$s за \"%3$s\""

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/comments.php:55
msgid "Name"
msgstr "Име"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/comments.php:56
msgid "E-mail"
msgstr "E-поща"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/comments.php:57
msgid "Website"
msgstr "Уебсайт"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/comments.php:61
msgid "Comment"
msgstr "Коментар"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/functions.php:92
#, php-format
msgid ""
"<span class=\"sep\">Posted on </span><a href=\"%1$s\" title=\"%2$s\" rel="
"\"bookmark\"><time class=\"entry-date\" datetime=\"%3$s\">%4$s</time></a>"
msgstr ""
"<span class=\"sep\">Публикувано на  </span><a href=\"%1$s\" title=\"%2$s\" "
"rel=\"bookmark\"><time class=\"entry-date\" datetime=\"%3$s\">%4$s</time></a>"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/functions.php:102
#, php-format
msgid ""
"<span class=\"sep\">Posted on </span><a href=\"%1$s\" title=\"%2$s\" rel="
"\"bookmark\"><time class=\"entry-date\" datetime=\"%3$s\">%4$s</time></"
"a><span class=\"by-author\"> <span class=\"sep\"> by </span> <span class="
"\"author vcard\"><a class=\"url fn n\" href=\"%5$s\" title=\"%6$s\" rel="
"\"author\">%7$s</a></span></span>"
msgstr ""
"<span class=\"sep\"> Публикувано на</span><a href=\"%1$s\" title=\"%2$s\" "
"rel=\"bookmark\"><time class=\"entry-date\" datetime=\"%3$s\">%4$s</time></"
"a><span class=\"by-author\"> <span class=\"sep\">от </span> <span class="
"\"author vcard\"><a class=\"url fn n\" href=\"%5$s\" title=\"%6$s\" rel="
"\"author\">%7$s</a></span></span>"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/functions.php:108
#, php-format
msgid "View all posts by %s"
msgstr "Виж всички мнения от %s"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/functions.php:135
msgid "Primary Widget Area"
msgstr "Основно Widget Area"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/functions.php:139
msgid "The primary widget area"
msgstr "Първична джаджа площ"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/functions.php:156
msgid "Secondary Widget Area"
msgstr "Средно Widget Area"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/functions.php:160
msgid "The secondary widget area"
msgstr "Площта на вторичния джаджа"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/functions.php:206
msgid "Primary Menu"
msgstr "Основно Меню"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/functions.php:421
msgid "Edit"
msgstr "Редактирам"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-contact.php:115
msgid "Full Name"
msgstr "Пълно име"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-contact.php:123
msgid "Your Email"
msgstr "Вашият Email"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-contact.php:131
msgid "Message title"
msgstr "Заглавие на Съобщението"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-contact.php:141
msgid "Comments"
msgstr "Коментари"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-contact.php:151
msgid "The Name field is required."
msgstr "Се изисква полето Име."

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-contact.php:151
msgid "The Email field is required."
msgstr "Се изисква Полето Email."

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-contact.php:151
msgid "Please provide a valid email address."
msgstr "Моля, въведете валиден имейл адрес."

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-contact.php:151
msgid "The Message field is required."
msgstr "Е необходимо полето на съобщението."

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-contact.php:151
msgid "Sorry, the code you entered was invalid."
msgstr "За съжаление, кодът, който сте въвели е невалиден."

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-contact.php:152
msgid "Reset"
msgstr "Reset"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-login.php:37
#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-login.php:45
msgid "Log in"
msgstr "Влезте в"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-login.php:41
msgid "Password"
msgstr "Парола"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-login.php:60
msgid "Log Out"
msgstr "Изход"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-product.php:74
msgid "Read More"
msgstr "Прочетете още"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-sitemap.php:45
msgid "Static Pages:"
msgstr "Статични страници:"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-sitemap.php:57
msgid "All Categories:"
msgstr "Всички категории:"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-sitemap.php:70
msgid "Tags:"
msgstr "Tags:"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-sitemap.php:87
msgid "Archives:"
msgstr "Archives:"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-sitemap.php:100
msgid "Authors:"
msgstr "Автори:"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/page-sitemap.php:113
msgid "Blog Posts:"
msgstr "Блог Публикации:"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/search.php:39
msgid "Count"
msgstr "Броене"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/single.php:34
msgid "Page"
msgstr "Страница"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/admin/color_control.php:320
msgid "Schemes"
msgstr "Схеми"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/admin/install_sampl_date.php:1314
msgid "Home"
msgstr "Вкъщи"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/admin/shortcode/tinymce/config.php:15
msgid "Insert Button Shortcode"
msgstr "Поставете бутон Shortcode"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/admin/shortcode/tinymce/config.php:25
msgid "Insert Link Shortcode"
msgstr "Поставете Link Shortcode"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/admin/shortcode/tinymce/config.php:39
msgid "Insert Info Box Shortcode"
msgstr "Поставете Info Box Shortcode"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/admin/shortcode/tinymce/config.php:50
msgid "Insert Quote Box Shortcode"
msgstr "Поставете Цитат Box Shortcode"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/admin/shortcode/tinymce/config.php:62
msgid "Insert Contact Form Shortcode"
msgstr "Поставете Форма за контакт Shortcode"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/admin/shortcode/tinymce/config.php:73
msgid "Insert Tabs Creator Shortcode"
msgstr "Поставете Tabs Creator Shortcode"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/admin/shortcode/tinymce/config.php:83
msgid "Insert Related Posts Shortcode"
msgstr "Поставете Подобни публикации Shortcode"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/admin/shortcode/tinymce/config.php:95
msgid "Insert Columns Layout Shortcode"
msgstr "Поставете Колони Layout Shortcode"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/admin/widgets/widget-category.php:77
msgid "More"
msgstr "Още"

#: C:\wamp\www\theme_test\wp-content\themes\exclusive/custom/seo_meta.php:22
msgid "SEO Exclusive"
msgstr "SEO Exclusive"
