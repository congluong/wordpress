<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php bloginfo('title') ?></title>
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
        <?php wp_head() ?>
    </head>
    <body>
        <div id="wrapper" <?php echo !is_home() ? 'class="wrapper-page wrapper-flexi"' : '' ?>>
            <!-- Start Header -->
            <div id="header">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <h1><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Convert" /></a></h1>
                            <h2 class="menulink"><a href="#">Menu</a></h2>
                            <!-- Start Menu -->
                           <!--  <div id="menu">
                                <ul>
                                    <li><a href="../index.html">Trang chủ</a></li>
                                    <li><a href="page.html">Giới thiệu</a></li>
                                    <li><a href="page.html">Tin tức</a></li>
                                    <li><a href="page.html">Bảng giá</a></li>
                                    <li class="current"><a href="page.html">Download App</a></li>
                                </ul>
                            </div> -->

                            <?php wp_nav_menu( array(
                                'theme_location' => 'main-nav', // tên location cần hiển thị
                                'container' => 'nav', // thẻ container của menu
                                'container_class' => 'main-nav', //class của container
                                'menu_class' => 'menu clearfix', // class của menu bên trong
                                'container_id' => 'menu'
                            ) ); ?>

                            <!-- End Menu -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

