    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="inner">
                    <div class="hero" style="display: block;">
                        <h1><?php the_title() ?></h1>
                        <h2>Quisque id tincidunt dui augue. Suspendisse non lorem vel diam consectetur posuere.</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="clearfix"></div>
    <div class="clearfix"></div>

</div><!-- CLOSE WRAPPER -->

<div class="strip" id="content">

    <div class="container">

        <div class="row">
            <div class="span12">
                <div class="inner">
                    <?php the_content() ?>
                </div>
            </div>
        </div>

    </div>

</div>