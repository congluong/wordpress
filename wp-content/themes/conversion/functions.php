<?php

add_theme_support( 'menus' );

register_nav_menus(
    array(
        'main-nav' => 'Menu chính'
    )
);

function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}

add_action('get_header', 'remove_admin_login_header');

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'current ';
    }
    return $classes;
}

function theme_conversion_add_styles() {
    wp_enqueue_style( 'contact', get_template_directory_uri() . '/css/contact.css', false );
    wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css', false );
    wp_enqueue_style( 'style-color', get_template_directory_uri() . '/css/colors/style-color-01.css', false );

    if( is_home() ) {
        wp_enqueue_style( 'picture', get_template_directory_uri() . '/css/style-picture.css', false );
    }

    wp_enqueue_style( 'line-icon', get_template_directory_uri() . '/css/simple-line-icons.css', false );
    wp_enqueue_style('fonts', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' );
}

add_action( 'wp_enqueue_scripts' , 'theme_conversion_add_styles');

function theme_conversion_add_scripts() {
    wp_enqueue_script( '_jquery', get_template_directory_uri() . '/js/jquery.min.js', false);
    wp_enqueue_script( 'respond', get_template_directory_uri() . '/js/respond.min.js', false);
    wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', false);
    wp_enqueue_script( 'jquery_form', get_template_directory_uri() . '/js/jquery.form.js', false);
    wp_enqueue_script( 'jquery_validate', get_template_directory_uri() . '/js/jquery.validate.min.js');
    wp_enqueue_script( 'contact', get_template_directory_uri() . '/js/contact.js' );
}

add_action( 'wp_enqueue_scripts' , 'theme_conversion_add_scripts');