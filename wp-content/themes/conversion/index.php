<?php get_header() ?>
            <!-- End Header -->
            <!-- Start Content -->
            <div class="container">
                <div class="row">
                    <div class="span6">
                        <div class="inner">
                            <div class="hero">
                                <h1>Thiết kế web trọn gói</h1>
                                <h2>Website đẹp, chất lượng, chuẩn SEO</h2>
                                <!-- <a href="#" class="btn btn-primary">Pricing</a> <a href="#" class="btn btn-secondary">Documentation</a> -->
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="inner">
                            <div class="form-box">
                                <div class="top">
                                    <h2><span class="glyph-item  icon-pencil" aria-hidden="true"></span> Get in touch</h2>
                                    <p>Vui lòng để lại thông tin để chúng tôi phục vụ bạn tốt hơn</p>
                                </div>
                                <div class="bottom">
                                    <div id="success">
                                        <span class="green textcenter">
                                            <p>Your message was sent successfully!</p>
                                        </span>
                                    </div>
                                    <div id="error">
                                        <span>
                                            <p>Something went wrong. Please refresh and try again.</p>
                                        </span>
                                    </div>
                                    <form id="contact" name="contact" method="post" novalidate="novalidate">
                                        <div class="form-row">
                                            <input type="text" name="name" id="name" size="30" value="" required="" class="text login_input"  placeholder="Họ và tên">
                                        </div>
                                        <div class="form-row">
                                            <input type="text" name="email" id="email" size="30" value="" required="" class="text login_input"  placeholder="Email">
                                        </div>
                                        <div class="form-row">
                                            <input type="text" name="phone" id="phone" size="30" value="" class="text login_input"  placeholder="Số điện thoại">
                                        </div>
                                        <div class="form-row">
                                            <textarea name="message" id="message" required=""  placeholder="Thông điệp"></textarea>
                                        </div>
                                        <div class="form-row">
                                            <input id="submit" type="submit" name="submit" value="Gửi thông điệp" class="btn">
                                            <!-- <a href="#">Privacy Policy</a> | <a href="#">Terms &amp; Conditions</a> -->
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="shadow"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End content -->
            <div class="clearfix"></div>
        </div>
        <div class="strip features">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <div class="inner">
                            <span class="glyph-item mega icon-screen-desktop" aria-hidden="true"></span>
                            <h3>Responsive</h3>
                            <p>Website tương thích với thiết bị di động là xu hướng tất yếu trong thời buổi kinh doanh hiện nay</p>
                        </div>
                    </div>
                    <div class="span3">
                        <div class="inner">
                            <span class="glyph-item mega icon-emoticon-smile" aria-hidden="true"></span>
                            <h3>Đơn giản &amp; dễ dàng</h3>
                            <p>Bạn chỉ cần gọi điện hoặc để lại tin nhắn. Nhân viên của chúng tôi sẽ ngay lập tức tư vấn cho bạn</p>
                        </div>
                    </div>
                    <div class="span3">
                        <div class="inner">
                            <span class="glyph-item mega icon-users" aria-hidden="true"></span>
                            <h3>Chăm sóc khách hàng</h3>
                            <p>Với phương châm khách hàng là số 1. Chúng tôi luôn đặt lợi ích và niềm vui của khách hàng lên đầu</p>
                        </div>
                    </div>
                    <div class="span3">
                        <div class="inner">
                            <span class="glyph-item mega icon-screen-smartphone" aria-hidden="true"></span>
                            <h3>Mobile Friendly</h3>
                            <p>Tối ưu hiển thị, SEO tốt trên các thiết bị di động</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="strip highlight strip-alt">
            <div class="container">
                <div class="row">
                    <div class="span6">
                        <div class="inner inner-text">
                            <h2>Hiển thị tốt trên <span>Mọi thiết bị</span></h2>
                            <h4>Phasellus consequat facilisis volutpat ma faucibus odio vitae semper. Ae volutpat lobortis. </h4>
                            <p>Mauris vehicula tortor id augue rutrum consequat ac at massa. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus bibendum urna pellentesque urna ultrices porttitor. Sed ac leo id odio fringilla venenatis. Morbi vel turpis luctus diam ultrices adipiscing. Vestibulum feugiat suscipit tortor, sed hendrerit ipsum ultricies eu. </p>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="inner">
                            <img src="<?php echo get_template_directory_uri() ?>/pictures/desktop-inline.png" alt="Desktop" class="promoimg" />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="strip highlight">
            <div class="container">
                <div class="row">
                    <div class="span6">
                        <div class="inner">
                            <img src="<?php echo get_template_directory_uri() ?>/pictures/mobile-inline.png" alt="Desktop" class="promoimg" />
                        </div>
                    </div>
                    <div class="span6">
                        <div class="inner inner-text">
                            <h2>Super easy to <span>customize</span></h2>
                            <h4>Phasellus consequat facilisis volutpat ma faucibus odio vitae semper. Ae volutpat lobortis. </h4>
                            <p>Mauris vehicula tortor id augue rutrum consequat ac at massa. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus bibendum urna pellentesque urna ultrices porttitor. Sed ac leo id odio fringilla venenatis. Morbi vel turpis luctus diam ultrices adipiscing. Vestibulum feugiat suscipit tortor, sed hendrerit ipsum ultricies eu. </p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- <div class="strip strip-alt pricing">
            <div class="container">
                <div class="row blocks">
                    <div class="span4">
                        <div class="inner secondary">
                            <ul>
                                <li>
                                    <h2>Cơ bản</h2>
                                </li>
                                <li class="price">
                                    <h1><span>$ </span><strong>12</strong><small> per month</small></h1>
                                </li>
                                <li>10GB Monthly Bandwidth</li>
                                <li>5GB Disk Space</li>
                                <li>20 E-mail accounts</li>
                                <li>8 subdomains</li>
                                <li>24/24 Ultimated support</li>
                            </ul>
                            <a href="#" class="btn">Buy Now</a>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="inner primary">
                            <ul>
                                <li>
                                    <h2>Premium</h2>
                                </li>
                                <li class="price">
                                    <h1><span>$ </span><strong>49</strong><small> per month</small></h1>
                                </li>
                                <li>10GB Monthly Bandwidth</li>
                                <li>5GB Disk Space</li>
                                <li>20 E-mail accounts</li>
                                <li>8 subdomains</li>
                                <li>24/24 Ultimated support</li>
                            </ul>
                            <a href="#" class="btn">Buy Now</a>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="inner secondary">
                            <ul>
                                <li>
                                    <h2>Standard</h2>
                                </li>
                                <li class="price">
                                    <h1><span>$ </span><strong>22</strong><small> per month</small></h1>
                                </li>
                                <li>10GB Monthly Bandwidth</li>
                                <li>5GB Disk Space</li>
                                <li>20 E-mail accounts</li>
                                <li>8 subdomains</li>
                                <li>24/24 Ultimated support</li>
                            </ul>
                            <a href="#" class="btn">Buy Now</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div> -->
<?php get_footer() ?>